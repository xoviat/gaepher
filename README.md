# Gaepher

No HTML. No CSS. No Javascript. Just Go.

Gaepher is *the* workflow tool that enables you to write all of your web applications
in Go without having to worry about deployment headaches. Gaepher is a thin wrapper
around exisiting technologies; namely, Google App Engine and WASM. Gaepher allows you
to create, develop, and deploy projects without having to write a single line of HTML,
CSS, or Javasript. And because all Gaepher applications run on Google App Engine, you
won't have to worry about deployment or maintenance headaches either.

Gaepher doesn't specify what libraries your project should use, or offer a specific
design philosophy for your web application. Gaepher only requires that your application
has two entry points; one for the client and one for the server. Everything else--
including how you share data and software between the client and the server--is up to
you.

## How it works

Behind the scenes, Gaepher watches for changes in both your client and server folders,
and recompiles the appropriate files where necessary. Gaepher wraps 'gcloud deploy', but
recompiles your application before deployment.

## Entry Points

Entry points are an **optional** feature of gaepher that simplifies development of your
web application. Whatever web framework you may prefer, your application almost certainly
communicates in some way with the server, probably using XMLHTTPRequests under the hood.

Rather than spending time developing public API from the server, and deciding on a routing
table that your client application can use, let gaepher handle the job for you!

**On the server,** in the "server" folder, create a new package called "entrypoints." Write
public, exportable go functions inside of this folder that accept and return serializable
arguments. That could be almost anything: strings, structs, times, whatever! Entrypoints,
however, cannot accept interfaces or arbitrary functions. This limitation removes possible 
vulnerabilities that arise from accepting unknown structs and arbirtrary functions; allowing
functions as arguments could allow arbitrariy code execution.

Then, run

```bash
gaepher generate
```

**On the client,** simply call the functions that gaepher has created! Gaepher generates
the glue code that handles serializing and deserializing arguments and returns.

**NOTICE:** Calling entry points on the client may block synchronous execution. The developer
should wrap all calls to entry points in a goroutine unless otherwise determined. In the case of
vecty, the developer should not call an entry point function as part of a render.

**SECURITY NOTICE:** All entry point functions are entry points from the outside web. You
should consider arguments to the functions to be unsafe and unsanitized. Gaepher will not
execute arbitrary code except to call the entry point function, but it is your responsibility
to ensure that the contents of the entry point function are safe.


## Helpful tips

Although Gaepher purposefully doesn't require any specific libraries, labstack Echo for
the server-side, and Vecty for the client side, have been two libraries that are a good
fit for writing full-stack web applications in Go.

To share code between the client and the server, simply create a new library and import
it into both your server and client web application, like you would with any project.
