package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/xoviat/gaepher/example/server/entrypoints"
	"google.golang.org/appengine"
)

func main() {
	entrypoints.Register()

	http.HandleFunc("/test", handle)
	appengine.Main()
}

func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}
