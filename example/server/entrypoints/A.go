package entrypoints

import (
	"context"

	"bitbucket.org/xoviat/gaepher/example/server/models"
)

func A(_ context.Context, t models.TestStruct) string {
	return t.FieldA + " " + t.FieldB
}

func B(_ context.Context, t models.TestModel) string {
	return t.FieldA + " " + t.FieldB
}

func C(_ context.Context, t models.TestModel) []models.TestModel {
	return []models.TestModel{{}}
}

func D(_ context.Context, t models.TestModel) []*models.TestModel {
	return []*models.TestModel{{}}
}
