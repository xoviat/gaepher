/*
	THIS FILE IS GENERATED BY GAEPHER. DO NOT EDIT!
*/

package entrypoints

import (
	"encoding/gob"
	"io"
	"net/http"
	"syscall/js"

	"bitbucket.org/xoviat/gaepher/example/server/models"
)

func A(param0 models.TestStruct) string {

	fetch := js.Global().Get("window").
		Get("location").Get("origin").String() + "/api/v8/7fc56270"
	rd, wr := io.Pipe()
	go func() {
		enc := gob.NewEncoder(wr)
		/*
			For each function argument, encode the result
		*/

		enc.Encode(param0)

		/*
			End for each
		*/

		_ = enc
		wr.Close()
	}()

	response, _ := http.Post(
		fetch, "binary/octet-stream", rd,
	)

	dec := gob.NewDecoder(response.Body)
	/*
		For each return, decode the result
	*/
	var ret0 string
	dec.Decode(&ret0)

	/*
		End for each
	*/
	_ = dec

	return ret0
}

func B(param0 models.TestModel) string {

	fetch := js.Global().Get("window").
		Get("location").Get("origin") + "/api/v8/9d5ed678"
	rd, wr := io.Pipe()
	go func() {
		enc := gob.NewEncoder(wr)
		/*
			For each function argument, encode the result
		*/

		enc.Encode(param0)

		/*
			End for each
		*/

		_ = enc
		wr.Close()
	}()

	response, _ := http.Post(
		fetch, "binary/octet-stream", rd,
	)

	dec := gob.NewDecoder(response.Body)
	/*
		For each return, decode the result
	*/
	var ret0 string
	dec.Decode(&ret0)

	/*
		End for each
	*/
	_ = dec

	return ret0
}

func C(param0 models.TestModel) []models.TestModel {

	fetch := js.Global().Get("window").
		Get("location").Get("origin") + "/api/v8/0d61f837"
	rd, wr := io.Pipe()
	go func() {
		enc := gob.NewEncoder(wr)
		/*
			For each function argument, encode the result
		*/

		enc.Encode(param0)

		/*
			End for each
		*/

		_ = enc
		wr.Close()
	}()

	response, _ := http.Post(
		fetch, "binary/octet-stream", rd,
	)

	dec := gob.NewDecoder(response.Body)
	/*
		For each return, decode the result
	*/
	var ret0 []models.TestModel
	dec.Decode(&ret0)

	/*
		End for each
	*/
	_ = dec

	return ret0
}

func D(param0 models.TestModel) []*models.TestModel {

	fetch := js.Global().Get("window").
		Get("location").Get("origin") + "/api/v8/f623e75a"
	rd, wr := io.Pipe()
	go func() {
		enc := gob.NewEncoder(wr)
		/*
			For each function argument, encode the result
		*/

		enc.Encode(param0)

		/*
			End for each
		*/

		_ = enc
		wr.Close()
	}()

	response, _ := http.Post(
		fetch, "binary/octet-stream", rd,
	)

	dec := gob.NewDecoder(response.Body)
	/*
		For each return, decode the result
	*/
	var ret0 []*models.TestModel
	dec.Decode(&ret0)

	/*
		End for each
	*/
	_ = dec

	return ret0
}
