package main

import (
	"bitbucket.org/xoviat/gaepher/example/client/entrypoints"
	"bitbucket.org/xoviat/gaepher/example/server/models"
)

func main() {

	entrypoints.A(models.TestStruct{
		FieldA: "A and ",
		FieldB: "B",
	})

}
