module bitbucket.org/xoviat/gaepher

go 1.12

require (
	github.com/LK4D4/trylock v0.0.0-20191027065348-ff7e133a5c54
	github.com/deckarep/golang-set v1.7.1
	github.com/fsnotify/fsnotify v1.5.1
	github.com/otiai10/copy v1.6.0 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/spf13/cast v1.4.1 // indirect
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	golang.org/x/sys v0.0.0-20210910150752-751e447fb3d0 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/ini.v1 v1.63.0 // indirect
)
