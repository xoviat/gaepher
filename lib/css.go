package lib

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type Stylesheet struct {
	fp io.Writer
}

func (ss *Stylesheet) AddFile(reader io.Reader) {
	/* todo: compression, style resolution */
	io.Copy(ss.fp, bufio.NewReader(reader))
	fmt.Fprintln(ss.fp, "")
}

/*
	src and dst must be an 'assets' dir
*/
func BuildCSS(src, dst string) error {
	os.MkdirAll(dst, 0777)

	fdst, err := os.Create(filepath.Join(dst, "styles.css"))
	if err != nil {
		return err
	}

	defer fdst.Close()
	files, err := filepath.Glob(filepath.Join(src, "styles", "*.css"))
	if err != nil {
		return err
	}
	ss := &Stylesheet{fp: fdst}

	for _, file := range files {
		fp, err := os.Open(file)
		if err != nil {
			return err
		}

		ss.AddFile(fp)
		fp.Close()
	}

	return nil
}
