package lib

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

func CopyFile(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func NewCommand(args []string, cwd string, environ []string) *exec.Cmd {
	fmt.Println(strings.Join(args, " "))

	cmd := exec.Command(args[0], args[1:]...)
	cmd.Env = append(os.Environ(), environ...)
	if cwd != "" {
		cmd.Dir = cwd
	}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd
}

func WriteIndex(w io.Writer) error {
	tmpl, err := template.New("index").Parse(INDEX_HTML)
	if err != nil {
		return err
	}

	data := struct {
		WASMFile    string
		LoaderImage string
	}{
		WASMFile:    "main.wasm",
		LoaderImage: LOADER_GIF,
	}

	fmt.Fprintln(w, "<!-- gaepher bootloader v0.1.0. copyright (c) 2020 Mars Galactic -->")

	return tmpl.Execute(w, data)
}

func BuildWASM(src, dst string, support bool) error {
	if support {
		index := filepath.Join(dst, "index.html")
		exec := filepath.Join(dst, "wasm_exec.js")

		buf, _ := ioutil.ReadFile(path.Join(runtime.GOROOT(), "misc/wasm/wasm_exec.js"))
		ioutil.WriteFile(exec, buf, 0777)

		fpi, _ := os.Create(index)
		WriteIndex(fpi)
		fpi.Close()
	}

	dst = filepath.Join(dst, "main.wasm")
	os.Remove(dst)

	NewCommand(
		[]string{
			filepath.Join(runtime.GOROOT(), "bin", "go"), "build", "-o", dst,
		},
		src,
		[]string{"GOOS=js", "GOARCH=wasm"},
	).Run()

	if _, err := os.Stat(dst); err != nil {
		return fmt.Errorf("failed to build main.wasm")
	}

	return nil
}

func ListDir(src string) []string {
	files, err := ioutil.ReadDir(src)
	if err != nil {
		return []string{}
	}

	fnames := make([]string, len(files))
	for i, f := range files {
		fnames[i] = f.Name()
	}

	return fnames
}

func IsDir(src string) bool {
	fi, err := os.Stat(src)
	if err != nil {
		return false
	}
	return fi.IsDir()
}

func IsFile(src string) bool {
	fi, err := os.Stat(src)
	if err != nil {
		return false
	}
	return !fi.IsDir()
}

type Directories struct {
	client string
	server string
}

func (d *Directories) IsLocal() bool {
	return d.server == ""
}

func (d *Directories) Client() string {
	return d.client
}

func (d *Directories) Server() string {
	return d.server
}

func GetDirectories() (*Directories, error) {
	cwd, _ := os.Getwd()
	client := filepath.Join(cwd, "client")
	server := filepath.Join(cwd, "server")

	var d *Directories
	if IsDir(server) && IsFile("app.yaml") {
		if !IsDir(client) {
			return nil, fmt.Errorf("client directory does not exist")
		}

		d = &Directories{client: client, server: server}
	} else {
		d = &Directories{client: cwd}
	}

	// check for go files in the client directory
	names, _ := filepath.Glob(filepath.Join(d.client, "*.go"))
	if len(names) == 0 {
		return nil, fmt.Errorf("no go files present in client directory")
	}

	// todo: check that go files have main package

	return d, nil
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func Replace2(s, olda, newa, oldb, newb string) string {
	return strings.Replace(strings.Replace(s, olda, newa, -1), oldb, newb, -1)
}

func Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func RemovePrefixes(prefix string) string {
	for i := 0; i < 2; i++ {
		prefix = strings.TrimPrefix(prefix, "[]")
		prefix = strings.TrimPrefix(prefix, "*")
	}

	return prefix
}
