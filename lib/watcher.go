package lib

import (
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/LK4D4/trylock"
	"github.com/fsnotify/fsnotify"
)

type WatchType int

const (
	Files WatchType = iota
	Folders
	FilesAndFolders
)

type FileWatcher interface {
	WatchFor(typ WatchType, callback func(string))
}

type Watcher struct {
	*fsnotify.Watcher
	recursive bool
	files     chan string
	folders   chan string
	errors    chan error
}

func NewWatcher(path string) (FileWatcher, error) {
	return newWatcher(path, false)
}

func NewRecursiveWatcher(path string) (FileWatcher, error) {
	return newWatcher(path, true)
}

func newWatcher(path string, recursive bool) (FileWatcher, error) {
	folders := []string{path}
	if recursive {
		folders = Subfolders(path)
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}
	rw := &Watcher{Watcher: watcher, recursive: recursive}

	rw.files = make(chan string, 10)
	rw.folders = make(chan string, len(folders))

	for _, folder := range folders {
		rw.AddFolder(folder)
	}
	return rw, nil
}

func (watcher *Watcher) AddFolder(folder string) {
	err := watcher.Add(folder)
	if err != nil {
		log.Println("Error watching: ", folder, err)
	}
	watcher.folders <- folder
}

func (watcher *Watcher) Run() {
	go func() {
		for {
			select {
			case event := <-watcher.Events:
				// create a file/directory
				if event.Op&fsnotify.Create == fsnotify.Create {
					fi, err := os.Stat(event.Name)
					if err != nil {
						// eg. stat .subl513.tmp : no such file or directory
					} else if watcher.recursive && fi.IsDir() {
						if !shouldIgnoreFile(filepath.Base(event.Name)) {
							watcher.AddFolder(event.Name)
						}
					} else {
						watcher.files <- event.Name // created a file
					}
				}

				if event.Op&fsnotify.Write == fsnotify.Write {
					// modified a file, assuming that you don't modify folders
					watcher.files <- event.Name
				}

			case err := <-watcher.errors:
				log.Println("error", err)
			}
		}
	}()
}

func (watcher *Watcher) WatchFor(typ WatchType, callback func(string)) {
	l := trylock.Mutex{}

	wcallback := func(path string) {
		go func() {
			if l.TryLock() {
				callback(path)
				time.Sleep(3 * time.Second)
				l.Unlock()
			}
		}()
	}

	fc, flc, _ := watcher.Channels()
	files := typ == Files || typ == FilesAndFolders
	folders := typ == Folders || typ == FilesAndFolders

	go func() {
		wcallback("")
		watcher.Run()
		for {
			select {
			case path := <-fc:
				if files {
					wcallback(path)
				}
			case path := <-flc:
				if folders {
					wcallback(path)
				}
			}
		}
	}()
}

func (watcher *Watcher) Channels() (chan string, chan string, chan error) {
	return watcher.files, watcher.files, watcher.errors
}

// Subfolders returns a slice of subfolders (recursive), including the folder provided.
func Subfolders(path string) (paths []string) {
	filepath.Walk(path, func(newPath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			name := info.Name()
			// skip folders that begin with a dot
			if shouldIgnoreFile(name) && name != "." && name != ".." {
				return filepath.SkipDir
			}
			paths = append(paths, newPath)
		}
		return nil
	})
	return paths
}

// shouldIgnoreFile determines if a file should be ignored.
// File names that begin with "." or "_" are ignored by the go tool.
func shouldIgnoreFile(name string) bool {
	return strings.HasPrefix(name, ".") || strings.HasPrefix(name, "_")
}
