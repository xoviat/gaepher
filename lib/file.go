package lib

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/printer"
	"strings"

	"errors"
	"go/parser"
	"go/token"
	"io/ioutil"

	mapset "github.com/deckarep/golang-set"
)

type Type struct {
	Prefix    string
	Name      string
	Rendering string
}

type Function struct {
	Name       string
	Parameters []Type
	Returns    []Type
}

type File struct {
	imports   []string
	Functions []Function
}

/*
	Create a list of type pairs from a field list
*/
func GetTypes(fieldlist *ast.FieldList, fset *token.FileSet) []Type {
	/*
		402  .  .  .  .  Results: *ast.FieldList {
		403  .  .  .  .  .  Opening: -
		404  .  .  .  .  .  List: []*ast.Field (len = 1) {
		405  .  .  .  .  .  .  0: *ast.Field {
		406  .  .  .  .  .  .  .  Type: *ast.ArrayType {
		407  .  .  .  .  .  .  .  .  Lbrack: 22:47
		408  .  .  .  .  .  .  .  .  Elt: *ast.SelectorExpr {
		409  .  .  .  .  .  .  .  .  .  X: *ast.Ident {
		410  .  .  .  .  .  .  .  .  .  .  NamePos: 22:49
		411  .  .  .  .  .  .  .  .  .  .  Name: "models"
		412  .  .  .  .  .  .  .  .  .  }
		413  .  .  .  .  .  .  .  .  .  Sel: *ast.Ident {
		414  .  .  .  .  .  .  .  .  .  .  NamePos: 22:56
		415  .  .  .  .  .  .  .  .  .  .  Name: "TestModel"
		416  .  .  .  .  .  .  .  .  .  }
		417  .  .  .  .  .  .  .  .  }
		418  .  .  .  .  .  .  .  }
		419  .  .  .  .  .  .  }
		420  .  .  .  .  .  }
		421  .  .  .  .  .  Closing: -
		422  .  .  .  .  }
	*/

	if fieldlist == nil {
		return []Type{}
	}

	pairs := make([]Type, len(fieldlist.List))
	for i, field := range fieldlist.List {
		var buf bytes.Buffer

		printer.Fprint(&buf, fset, field.Type)

		rendering := buf.String()
		names := strings.Split(rendering, ".")

		prefix := ""
		name := ""
		if len(names) == 1 {
			name = names[0]
		} else if len(names) == 2 {
			prefix = names[0]
			name = names[1]
		} else {
			panic("unexpected type name")
		}

		pairs[i] = Type{
			Prefix:    prefix,
			Name:      name,
			Rendering: rendering,
		}
	}

	return pairs
}

func (lf *File) Load(fname string) error {
	pkgname := "entrypoints"

	src, err := ioutil.ReadFile(fname)
	if err != nil {
		return err
	}

	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, "", src, 0)
	if err != nil {
		return err
	}

	// ast.Print(fset, f)

	if f.Name.Name != pkgname {
		return errors.New("package name confirmed as entrypoints")
	}

	for _, decl := range f.Decls {
		fdecl, ok := decl.(*ast.GenDecl)
		if !ok {
			continue
		}

		for _, spec := range fdecl.Specs {
			ispec, ok := spec.(*ast.ImportSpec)
			if !ok {
				continue
			}

			lf.imports = append(lf.imports, ispec.Path.Value)
		}
	}

	fmt.Printf("load file %s\n", fname)
	for _, decl := range f.Decls {
		fdecl, ok := decl.(*ast.FuncDecl)
		if !ok {
			continue
		}

		/*
			Is function exported?
		*/
		name := fdecl.Name.Name
		if string(name[0]) != strings.ToUpper(string(name[0])) {
			continue
		}

		ppairs := GetTypes(fdecl.Type.Params, fset)
		rpairs := GetTypes(fdecl.Type.Results, fset)

		if len(ppairs) < 1 || ppairs[0].Rendering != "context.Context" {
			/* no context: not an entry point */
			continue
		}
		ppairs = ppairs[1:]

		fmt.Printf("load function %s\n", name)
		lf.Functions = append(lf.Functions, Function{
			Name:       name,
			Parameters: ppairs,
			Returns:    rpairs,
		})
	}

	return nil
}

/*
	Get a list of the imports in the file
*/
func (lf *File) Imports() []string {
	set := mapset.NewSet()
	for _, fn := range lf.Functions {
		for _, pair := range fn.Parameters {
			set.Add(RemovePrefixes(pair.Prefix))
		}
		for _, pair := range fn.Returns {
			set.Add(RemovePrefixes(pair.Prefix))
		}
	}

	iset := mapset.NewSet()
	for _, imp := range lf.imports {
		imp = strings.TrimSuffix(imp, "\"")
		imp = strings.TrimPrefix(imp, "\"")

		parts := strings.Split(imp, "/")
		suffix := parts[len(parts)-1]

		if set.Contains(suffix) {
			iset.Add(imp)
		}
	}

	var imps []string
	it := iset.Iterator()
	for imp := range it.C {
		imps = append(imps, imp.(string))
	}

	return imps
}

/*
    0  *ast.File {
     1  .  Package: 1:1
     2  .  Name: *ast.Ident {
     3  .  .  NamePos: 1:9
     4  .  .  Name: "entrypoints"
     5  .  }
     6  .  Decls: []ast.Decl (len = 2) {

	 7  .  .  0: *ast.GenDecl {
     8  .  .  .  TokPos: 3:1
     9  .  .  .  Tok: import
    10  .  .  .  Lparen: -
    11  .  .  .  Specs: []ast.Spec (len = 1) {
    12  .  .  .  .  0: *ast.ImportSpec {
    13  .  .  .  .  .  Path: *ast.BasicLit {
    14  .  .  .  .  .  .  ValuePos: 3:8
    15  .  .  .  .  .  .  Kind: STRING
    16  .  .  .  .  .  .  Value: "\"bitbucket.org/xoviat/gaepher/example/server/models\""
    17  .  .  .  .  .  }
    18  .  .  .  .  .  EndPos: -
    19  .  .  .  .  }
    20  .  .  .  }
    21  .  .  .  Rparen: -
    22  .  .  }

	71  .  .  1: *ast.FuncDecl {
    72  .  .  .  Name: *ast.Ident {
    73  .  .  .  .  NamePos: 8:6
    74  .  .  .  .  Name: "A"
    75  .  .  .  .  Obj: *ast.Object {
    76  .  .  .  .  .  Kind: func
    77  .  .  .  .  .  Name: "A"
    78  .  .  .  .  .  Decl: *(obj @ 71)
    79  .  .  .  .  }
    80  .  .  .  }
    81  .  .  .  Type: *ast.FuncType {
    82  .  .  .  .  Func: 8:1
    83  .  .  .  .  Params: *ast.FieldList {
    84  .  .  .  .  .  Opening: 8:7
    85  .  .  .  .  .  List: []*ast.Field (len = 1) {
    86  .  .  .  .  .  .  0: *ast.Field {
    87  .  .  .  .  .  .  .  Names: []*ast.Ident (len = 1) {
    88  .  .  .  .  .  .  .  .  0: *ast.Ident {
    89  .  .  .  .  .  .  .  .  .  NamePos: 8:8
    90  .  .  .  .  .  .  .  .  .  Name: "t"
    91  .  .  .  .  .  .  .  .  .  Obj: *ast.Object {
    92  .  .  .  .  .  .  .  .  .  .  Kind: var
    93  .  .  .  .  .  .  .  .  .  .  Name: "t"
    94  .  .  .  .  .  .  .  .  .  .  Decl: *(obj @ 86)
    95  .  .  .  .  .  .  .  .  .  }
    96  .  .  .  .  .  .  .  .  }
    97  .  .  .  .  .  .  .  }
    98  .  .  .  .  .  .  .  Type: *ast.Ident {
    99  .  .  .  .  .  .  .  .  NamePos: 8:10
   100  .  .  .  .  .  .  .  .  Name: "TestStruct"
   101  .  .  .  .  .  .  .  .  Obj: *(obj @ 16)
   102  .  .  .  .  .  .  .  }
   103  .  .  .  .  .  .  }
   104  .  .  .  .  .  }
   105  .  .  .  .  .  Closing: 8:20
   106  .  .  .  .  }

   195  .  .  .  .  Params: *ast.FieldList {
   196  .  .  .  .  .  Opening: 14:7
   197  .  .  .  .  .  List: []*ast.Field (len = 1) {
   198  .  .  .  .  .  .  0: *ast.Field {
   199  .  .  .  .  .  .  .  Names: []*ast.Ident (len = 1) {
   200  .  .  .  .  .  .  .  .  0: *ast.Ident {
   201  .  .  .  .  .  .  .  .  .  NamePos: 14:8
   202  .  .  .  .  .  .  .  .  .  Name: "t"
   203  .  .  .  .  .  .  .  .  .  Obj: *ast.Object {
   204  .  .  .  .  .  .  .  .  .  .  Kind: var
   205  .  .  .  .  .  .  .  .  .  .  Name: "t"
   206  .  .  .  .  .  .  .  .  .  .  Decl: *(obj @ 198)
   207  .  .  .  .  .  .  .  .  .  }
   208  .  .  .  .  .  .  .  .  }
   209  .  .  .  .  .  .  .  }
   210  .  .  .  .  .  .  .  Type: *ast.SelectorExpr {
   211  .  .  .  .  .  .  .  .  X: *ast.Ident {
   212  .  .  .  .  .  .  .  .  .  NamePos: 14:10
   213  .  .  .  .  .  .  .  .  .  Name: "models"
   214  .  .  .  .  .  .  .  .  }
   215  .  .  .  .  .  .  .  .  Sel: *ast.Ident {
   216  .  .  .  .  .  .  .  .  .  NamePos: 14:17
   217  .  .  .  .  .  .  .  .  .  Name: "TestModel"
   218  .  .  .  .  .  .  .  .  }
   219  .  .  .  .  .  .  .  }
   220  .  .  .  .  .  .  }
   221  .  .  .  .  .  }
   222  .  .  .  .  .  Closing: 14:26
   223  .  .  .  .  }

   107  .  .  .  .  Results: *ast.FieldList {
   108  .  .  .  .  .  Opening: -
   109  .  .  .  .  .  List: []*ast.Field (len = 1) {
   110  .  .  .  .  .  .  0: *ast.Field {
   111  .  .  .  .  .  .  .  Type: *ast.Ident {
   112  .  .  .  .  .  .  .  .  NamePos: 8:22
   113  .  .  .  .  .  .  .  .  Name: "string"
   114  .  .  .  .  .  .  .  }
   115  .  .  .  .  .  .  }
   116  .  .  .  .  .  }
   117  .  .  .  .  .  Closing: -
   118  .  .  .  .  }
   119  .  .  .  }

   179  }
*/
