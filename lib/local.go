package lib

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"
	"time"
)

type wasmServer struct {
	wasmExecJS []byte
}

func NewWASMServer() (http.Handler, error) {
	var err error
	srv := &wasmServer{}

	buf, err := ioutil.ReadFile(path.Join(runtime.GOROOT(), "misc/wasm/wasm_exec.js"))
	if err != nil {
		return nil, err
	}
	srv.wasmExecJS = buf

	return srv, nil
}

func (ws *wasmServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// log.Println(r.URL.Path)
	switch r.URL.Path {
	case "/main.wasm":
		f, err := os.Open("main.wasm")
		if err != nil {
			fmt.Println(err)
			return
		}
		defer func() {
			err := f.Close()
			if err != nil {
				fmt.Println(err)
			}
		}()
		http.ServeContent(w, r, r.URL.Path, time.Now(), f)

		return
	case "/wasm_exec.js":
		w.Header().Set("Content-Type", "application/javascript")
		w.Header().Set("Content-Length", strconv.Itoa(len(ws.wasmExecJS)))
		if _, err := w.Write(ws.wasmExecJS); err != nil {
			fmt.Println("unable to write wasm_exec.")
		}

		return
	}

	if !strings.HasPrefix(r.URL.Path, "/assets/") {
		w.Header().Set("Content-Type", "text/html; charset=UTF-8")
		if err := WriteIndex(w); err != nil {
			fmt.Println(err)
		}
	} else {
		f, err := os.Open(strings.TrimPrefix(r.URL.Path, "/"))
		if err != nil {
			fmt.Println(err)
			return
		}
		defer func() {
			err := f.Close()
			if err != nil {
				fmt.Println(err)
			}
		}()

		http.ServeContent(w, r, r.URL.Path, time.Now(), f)
	}
}
