// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"bitbucket.org/xoviat/gaepher/lib"
	"github.com/spf13/cobra"
)

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate entry point scaffolding",
	Long: `Read and interpret entry point functions, and generate the
scaffolding required to call them from the client.`,
	Run: func(cmd *cobra.Command, args []string) {
		directories, err := lib.GetDirectories()
		if err != nil {
			fmt.Printf("error: %s\n", err)
			return
		}

		generated_name := "generated-gaepher.go"
		pkgname := "entrypoints"

		sep := filepath.Join(directories.Server(), pkgname)
		cep := filepath.Join(directories.Client(), pkgname)

		os.MkdirAll(sep, 0777)
		os.MkdirAll(cep, 0777)

		fsep, err := os.Create(filepath.Join(sep, generated_name))
		if err != nil {
			fmt.Printf("error: %s\n", err)
			return
		}

		fcep, err := os.Create(filepath.Join(cep, generated_name))
		if err != nil {
			fmt.Printf("error: %s\n", err)
			return
		}

		defer fsep.Close()
		defer fcep.Close()

		lf := lib.File{}

		for _, fname := range lib.ListDir(sep) {
			if fname == generated_name {
				continue
			}

			lf.Load(filepath.Join(sep, fname))
		}

		fnimports := ""
		for _, imp := range lf.Imports() {
			fnimports += "    \"" + imp + "\"\n"
		}

		fmt.Fprint(fsep, lib.Replace2(lib.SEP_HEADER, "{fnimports}", fnimports, "", ""))
		fmt.Fprint(fcep, lib.Replace2(lib.CEP_HEADER, "{fnimports}", fnimports, "", ""))

		for _, fn := range lf.Functions {
			name := fn.Name
			fnurl := "/api/v8/" + lib.Hash(name)[:8]
			fnsig := name + "("     // Generated for client
			fncall := fnsig + "ctx" // Generated for server
			fnreturn := ""          // Generated for client

			if fn.Name == "LogError" || fn.Name == "IsAuthenticated" {
				continue
			}

			if len(fn.Parameters) > 0 {
				fncall += ", "
			}

			/*
				We must iterate twice to build the function signature
			*/
			for i, pair := range fn.Parameters {
				fnsig += "param" + strconv.Itoa(i) + " " + pair.Rendering
				fncall += "param" + strconv.Itoa(i)

				if i < len(fn.Parameters)-1 {
					fnsig += ", "
					fncall += ", "
				}
			}

			fnsig += ") "
			fncall += ")"
			if len(fn.Returns) > 1 {
				fnsig += "("
			}

			for i, pair := range fn.Returns {
				fnsig += pair.Rendering
				fnreturn += "ret" + strconv.Itoa(i)
				if i < len(fn.Returns)-1 {
					fnsig += ", "
					fnreturn += ", "
				}
			}
			fncall = fnreturn + " := " + fncall

			if len(fn.Returns) > 1 {
				fnsig += ")"
			}

			/*

				fmt.Println("signature " + fnsig)
				fmt.Println("fncall " + fncall)
				fmt.Println("fnreturn " + fnreturn)
				fmt.Println("url " + fnurl)
			*/

			fmt.Fprintln(fsep, lib.Replace2(lib.SEP_FUNCHEADER, "{fnsig}", fnsig, "{fnurl}", fnurl))
			fmt.Fprintln(fcep, lib.Replace2(lib.CEP_FUNCHEADER, "{fnsig}", fnsig, "{fnurl}", fnurl))

			/*
				fmt.Println("name is " + name)
			*/
			for i, pair := range fn.Parameters {
				param := "param" + strconv.Itoa(i)

				/*
					If there are user-defined types that exist in the server package, then we don't need to prefix them in the
					server package with "entrypoints.", but we need to import them from the server package in the client package.
				*/
				fmt.Fprint(fsep, lib.Replace2(lib.SEP_FUNCPROLOGUE, "{param}", param, "{ptype}", pair.Rendering))
				fmt.Fprint(fcep, lib.Replace2(lib.CEP_FUNCPROLOGUE, "{param}", param, "{ptype}", pair.Rendering))
			}

			fmt.Fprint(fsep, lib.Replace2(lib.SEP_FUNCCORE, "{fncall}", fncall, "{fnreturn}", fnreturn))
			fmt.Fprint(fcep, lib.Replace2(lib.CEP_FUNCCORE, "{fncall}", fncall, "{fnreturn}", fnreturn))

			for i, pair := range fn.Returns {
				param := "ret" + strconv.Itoa(i)
				sep_funcepilogue := ""
				if strings.HasPrefix(pair.Rendering, "*") ||
					strings.HasPrefix(pair.Rendering, "[") ||
					strings.HasPrefix(pair.Rendering, "map[") ||
					strings.HasPrefix(pair.Rendering, "error") {
					sep_funcepilogue = lib.SEP_PFUNCEPILOGUE
				} else {
					sep_funcepilogue = lib.SEP_FUNCEPILOGUE
				}

				fmt.Fprint(fsep, lib.Replace2(sep_funcepilogue, "{param}", param, "{ptype}", pair.Rendering))
				fmt.Fprint(fcep, lib.Replace2(lib.CEP_FUNCEPILOGUE, "{param}", param, "{ptype}", pair.Rendering))
			}

			if len(fn.Returns) > 0 && fn.Returns[len(fn.Returns)-1].Name == "error" {
				param := "ret" + strconv.Itoa(len(fn.Returns)-1)
				pair := fn.Returns[len(fn.Returns)-1]

				fmt.Fprint(fsep, lib.Replace2(lib.SEP_FUNCEPILOGUE_LOGERR, "{param}", param, "{ptype}", pair.Rendering))
				fmt.Fprint(fcep, lib.Replace2(lib.CEP_FUNCEPILOGUE_LOGERR, "{param}", param, "{ptype}", pair.Rendering))
			}

			fmt.Fprint(fsep, lib.Replace2(lib.SEP_FUNCFOOTER, "{fncall}", fncall, "{fnreturn}", fnreturn))
			fmt.Fprint(fcep, lib.Replace2(lib.CEP_FUNCFOOTER, "{fncall}", fncall, "{fnreturn}", fnreturn))
		}

		fmt.Fprint(fsep, lib.SEP_FOOTER)
		fmt.Fprint(fcep, lib.CEP_FOOTER)

		/*
			files, err := ioutil.ReadDir(sep)
			if err != nil {
				panic(err)
			}

			for _, f := range files {
				if !strings.HasSuffix(f.Name(), ".go") {
					continue
				}

				os.Remove(f.Name())
			}
		*/

	},
}

func init() {
	rootCmd.AddCommand(generateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}
