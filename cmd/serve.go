// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"io"
	"net/http"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"

	"github.com/otiai10/copy"
	"github.com/spf13/cobra"

	"os"
	"os/signal"

	"bitbucket.org/xoviat/gaepher/lib"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "serve a go web application on a local webserver",
	Long: `
gaepher serve should be executed from the root of your project, which should
contain the following directory structure if developing a google cloud app:
	- client
		- Contains a main package with an entry point for your client code
	- server
		- app.yaml
		- Contains a main package with an entry point for your server code

otherwise, gaepher serve should be executed in a client folder if developing
a purely 'client' web app.		
`,
	Run: func(cmd *cobra.Command, args []string) {
		directories, err := lib.GetDirectories()
		if err != nil {
			fmt.Printf("error: %s\n", err)
			return
		}

		if directories.IsLocal() {
			fmt.Println("no appengine directory found; running in local mode")

			if err := lib.BuildWASM("", "", false); err != nil {
				fmt.Printf("error %s\n", err)
				return
			}

			handler, _ := lib.NewWASMServer()
			port := 8080

			fmt.Println("listening on port " + strconv.Itoa(port))
			fmt.Println("open http://localhost:" + strconv.Itoa(port) + "/ to view")
			http.ListenAndServe("127.0.0.1:"+strconv.Itoa(port), handler)
			return
		}

		client := directories.Client()
		server := directories.Server()

		rebuild := func(path string) {
			path, _ = filepath.Rel(client, path)
			path = strings.ReplaceAll(path, "\\", "/")
			fmt.Println("detected file changes: " + path)
			if strings.HasPrefix(path, "assets/styles") {
				fmt.Println("rebuilding styles")
				lib.BuildCSS(
					filepath.Join(client, "assets"),
					filepath.Join(server, "assets"),
				)
			} else {
				fmt.Println("rebuilding client application")
				lib.BuildWASM(client, server, true)
			}
		}

		src, _ := os.Open(path.Join(runtime.GOROOT(), "misc/wasm/wasm_exec.js"))
		dst, _ := os.Create(path.Join(directories.Server(), "wasm_exec.js"))
		io.Copy(dst, src)
		src.Close()
		dst.Close()

		dst, _ = os.Create(path.Join(directories.Server(), "index.html"))
		lib.WriteIndex(dst)
		dst.Close()

		os.RemoveAll(filepath.Join(directories.Server(), "assets"))
		copy.Copy(
			filepath.Join(directories.Client(), "assets"),
			filepath.Join(directories.Server(), "assets"),
		)
		os.RemoveAll(filepath.Join(directories.Server(), "assets", "styles"))

		cw, err := lib.NewRecursiveWatcher(client)
		if err != nil {
			panic(err)
		}
		cw.WatchFor(lib.FilesAndFolders, rebuild)
		go func() { rebuild(filepath.Join(client, "assets/styles/a.css")) }()

		gcmd := lib.NewCommand(
			[]string{
				"C:\\Program Files (x86)\\Google\\Cloud SDK\\google-cloud-sdk\\platform\\bundledpython\\python.exe",
				"C:\\Program Files (x86)\\Google\\Cloud SDK\\google-cloud-sdk\\bin\\dev_appserver.py",
				filepath.Dir(server),
			},
			"",
			[]string{},
		)
		gcmd.Start()

		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)

		go func() {
			<-c // Block execution of goroutine until sigint is received

			fmt.Println("terminating application...")
			gcmd.Process.Signal(os.Interrupt)
		}()
		gcmd.Wait()
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serveCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}
